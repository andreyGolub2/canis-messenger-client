import { LocalStorageProxy } from "./LocalStorageProxy";

let _localStorageProxy: LocalStorageProxy;

export const localStorageProxy = () => {
    if (!_localStorageProxy) {
        _localStorageProxy = new LocalStorageProxy();
    }
    return _localStorageProxy;
};