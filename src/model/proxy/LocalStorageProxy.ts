import { LocalStorage } from "node-localstorage";

const LOCATION: string = "user-info";

export class LocalStorageProxy {

    private localStorage: LocalStorage;

    public constructor () {
        this.localStorage = new LocalStorage(LOCATION);
    }

    public get(key: string): string {
        return this.localStorage.getItem(key);
    }

    public set(key: string, value: string) {
        this.localStorage.setItem(key, value);
    }
}