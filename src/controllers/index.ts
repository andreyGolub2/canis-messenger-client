import { Controller } from "./Controller";
import  * as proxies from "../model/proxy";

let _controller: Controller;

export const controller = () => {
    if (!_controller) {
        _controller = new Controller(proxies.localStorageProxy());
    }
    return _controller;
};