import { app, BrowserWindow, App } from "electron";

class Application {
    private app: App;
    private mainWindow: BrowserWindow;

    private constructor() {
        this.initialize();
    }

    public static bootstrap(): Application {
        return new Application();
    }

    public initialize() {
        this.initApplication();
    }
    
    private initApplication() {
        this.app = app;
        this.app.on("ready", () => {
            this.createWindow();
        });
        this.app.on("window-all-closed", () => {
            app.quit();
        });
    }

    private createWindow() {
        this.mainWindow = new BrowserWindow({
            width: 900,
            height: 500,
            webPreferences: {
              nodeIntegration: true,
            },
            autoHideMenuBar: true,
            frame: false,
            title: "Canis Messenger",
            titleBarStyle: "hidden",
        });
        this.mainWindow.loadFile("html/index.html");
        this.mainWindow.on("closed", () => {
            this.mainWindow = null;
        });

    }
}

const application = Application.bootstrap();