import * as controllers from "../controllers";
import { Controller } from "../controllers/Controller";

const { dialog } = require("electron").remote;

class RendererProcess {
    private controller: Controller;

    public constructor() {
        this.init();
    }

    private init() {
        this.controller = controllers.controller();
        dialog.showOpenDialog({});
    }
    
    public static bootstrap(): RendererProcess {
        return new RendererProcess();
    }
}

const process = RendererProcess.bootstrap();